import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LeftMenuComponent } from './component/left-menu/left-menu.component';
import { MainContainerComponent } from './component/main-container/main-container.component';
import { HeaderComponent } from './component/main-container/header/header.component';
import { ApplicationDataComponent } from './component/main-container/application-data/application-data.component';
import { ApplicationDetailComponent } from './component/main-container/application-detail/application-detail.component';
import { ChildHeaderComponent } from './component/main-child-container/child-header/child-header.component';
import { ApplicationToolBarComponent } from './component/main-child-container/application-tool-bar/application-tool-bar.component';
import { ApplicationToolBarDetailComponent } from './component/main-child-container/application-tool-bar-detail/application-tool-bar-detail.component';
import { ApplicationAttachmentsComponent } from './component/main-container/application-attachments/application-attachments.component';
import { MainChildContainerComponent } from './component/main-child-container/main-child-container.component';
import { EmailComponent } from './component/main-container/email/email.component';
import { SingleDocumentComponent } from './component/main-container/single-document/single-document.component';
import { StructureDataComponent } from './component/main-container/structure-data/structure-data.component';
import { DetailAttachmentBarComponent } from './component/main-container/detail-attachment-bar/detail-attachment-bar.component';


@NgModule({
  declarations: [
    AppComponent,
    LeftMenuComponent,
    MainContainerComponent,
    HeaderComponent,
    ApplicationDataComponent,
    ApplicationDetailComponent,
    ChildHeaderComponent,
    ApplicationToolBarComponent,
    ApplicationToolBarDetailComponent,
    ApplicationAttachmentsComponent,
    MainChildContainerComponent,
    EmailComponent,
    SingleDocumentComponent,
    StructureDataComponent,
    DetailAttachmentBarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
