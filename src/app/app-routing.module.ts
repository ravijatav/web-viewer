import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Statement } from '@angular/compiler';
import { AppComponent } from './app.component';

import { MainContainerComponent } from './component/main-container/main-container.component';
import { MainChildContainerComponent } from './component/main-child-container/main-child-container.component';
import { EmailComponent } from './component/main-container/email/email.component';
import { SingleDocumentComponent } from './component/main-container/single-document/single-document.component';
import { StructureDataComponent } from './component/main-container/structure-data/structure-data.component';

const routes: Routes = [
  //  {path:'statement',
  //   loadChildren:  () => import('./statement/statement.module').then(m => m.StatementModule)}
  {
    path: 'statement', component: MainChildContainerComponent
  },
  {
    path: '', component: MainContainerComponent
  },
  {
    path: 'email', component: EmailComponent
  },
  {
    path:'single-document', component:SingleDocumentComponent
  },
  {
    path:'Structure-data', component:StructureDataComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
