import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detail-attachment-bar',
  templateUrl: './detail-attachment-bar.component.html',
  styleUrls: ['./detail-attachment-bar.component.scss']
})
export class DetailAttachmentBarComponent implements OnInit {

  detail =true;
  attachment=false;
  constructor() { }

  ngOnInit(): void {

  }
  details(){
    this.detail = !this.detail;
    this.attachment = !this.attachment;

  }
  attachments(){
    this.detail = !this.detail;
    this.attachment = !this.attachment;
  }

}
