import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAttachmentBarComponent } from './detail-attachment-bar.component';

describe('DetailAttachmentBarComponent', () => {
  let component: DetailAttachmentBarComponent;
  let fixture: ComponentFixture<DetailAttachmentBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailAttachmentBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailAttachmentBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
