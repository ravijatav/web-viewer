import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-main-container',
  templateUrl: './main-container.component.html',
  styleUrls: ['./main-container.component.scss']
})
export class MainContainerComponent implements OnInit {

detail =true;
attachment=false;
  constructor() { }

  ngOnInit(): void {

  }
  details(){
    this.detail = !this.detail;
    this.attachment = !this.attachment;

  }
  attachments(){
    this.detail = !this.detail;
    this.attachment = !this.attachment;
  }
}
