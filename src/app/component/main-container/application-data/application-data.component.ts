import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-application-data',
  templateUrl:'./application-data.component.html',
  styleUrls: ['./application-data.component.scss']
})
export class ApplicationDataComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }
  getOpneStatement( ){
    this.router.navigate(['statement']);
  }
}
