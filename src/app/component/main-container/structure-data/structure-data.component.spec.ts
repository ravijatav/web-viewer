import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StructureDataComponent } from './structure-data.component';

describe('StructureDataComponent', () => {
  let component: StructureDataComponent;
  let fixture: ComponentFixture<StructureDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StructureDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StructureDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
