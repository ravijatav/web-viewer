import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-child-header',
  templateUrl: './child-header.component.html',
  styleUrls: ['./child-header.component.scss']
})
export class ChildHeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  getback(){
    this.router.navigate(['']);
  }
}
