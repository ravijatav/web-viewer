import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainChildContainerComponent } from './main-child-container.component';

describe('MainChildContainerComponent', () => {
  let component: MainChildContainerComponent;
  let fixture: ComponentFixture<MainChildContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainChildContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainChildContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
