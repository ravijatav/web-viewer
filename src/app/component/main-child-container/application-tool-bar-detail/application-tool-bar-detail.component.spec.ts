import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationToolBarDetailComponent } from './application-tool-bar-detail.component';

describe('ApplicationToolBarDetailComponent', () => {
  let component: ApplicationToolBarDetailComponent;
  let fixture: ComponentFixture<ApplicationToolBarDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationToolBarDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationToolBarDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
